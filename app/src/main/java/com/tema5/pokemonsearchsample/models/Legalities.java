
package com.tema5.pokemonsearchsample.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Legalities {

    @SerializedName("unlimited")
    @Expose
    private String unlimited;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Legalities() {
    }

    /**
     * 
     * @param unlimited
     */
    public Legalities(String unlimited) {
        super();
        this.unlimited = unlimited;
    }

    public String getUnlimited() {
        return unlimited;
    }

    public void setUnlimited(String unlimited) {
        this.unlimited = unlimited;
    }

}
