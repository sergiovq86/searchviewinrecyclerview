
package com.tema5.pokemonsearchsample.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cards {

    @SerializedName("data")
    @Expose
    private List<Pokemon> data = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Cards() {
    }

    /**
     * 
     * @param data
     */
    public Cards(List<Pokemon> data) {
        super();
        this.data = data;
    }

    public List<Pokemon> getData() {
        return data;
    }

    public void setData(List<Pokemon> data) {
        this.data = data;
    }

}
