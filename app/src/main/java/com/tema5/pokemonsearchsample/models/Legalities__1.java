
package com.tema5.pokemonsearchsample.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Legalities__1 {

    @SerializedName("unlimited")
    @Expose
    private String unlimited;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Legalities__1() {
    }

    /**
     * 
     * @param unlimited
     */
    public Legalities__1(String unlimited) {
        super();
        this.unlimited = unlimited;
    }

    public String getUnlimited() {
        return unlimited;
    }

    public void setUnlimited(String unlimited) {
        this.unlimited = unlimited;
    }

}
