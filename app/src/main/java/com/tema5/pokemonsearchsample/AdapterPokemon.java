package com.tema5.pokemonsearchsample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.tema5.pokemonsearchsample.models.Images__1;
import com.tema5.pokemonsearchsample.models.Pokemon;

import java.util.ArrayList;
import java.util.List;

public class AdapterPokemon extends RecyclerView.Adapter<AdapterPokemon.PokeHolder> {

    private List<Pokemon> listaPoke;
    private List<Pokemon> listaCopia;
    private final Context context;

    public AdapterPokemon(Context context) {
        this.listaPoke = new ArrayList<>();
        this.listaCopia = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public PokeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false);
        return new PokeHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PokeHolder holder, int position) {
        Pokemon pokemon = listaPoke.get(position);
        holder.tvTitle.setText(pokemon.getName());
        List<String> tipos = pokemon.getSubtypes();
        holder.tvType.setText(tipos.get(0));
        Images__1 images = pokemon.getImages();
        Picasso.get().load(images.getSmall()).into(holder.ivImagen);
    }


    @Override
    public int getItemCount() {
        return listaPoke.size();
    }

    public void setDatos(List<Pokemon> lista) {
        listaPoke = lista;
        listaCopia = lista;
        notifyDataSetChanged();
    }

    static class PokeHolder extends RecyclerView.ViewHolder {

        private final TextView tvTitle;
        private final TextView tvType;
        private final ImageView ivImagen;

        PokeHolder(@NonNull View itemView) {
            super(itemView);
            this.tvTitle = itemView.findViewById(R.id.tvName);
            this.tvType = itemView.findViewById(R.id.tvType);
            ivImagen = itemView.findViewById(R.id.ivImage);
        }
    }
}
