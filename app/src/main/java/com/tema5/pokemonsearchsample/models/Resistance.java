
package com.tema5.pokemonsearchsample.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Resistance {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("value")
    @Expose
    private String value;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Resistance() {
    }

    /**
     * 
     * @param type
     * @param value
     */
    public Resistance(String type, String value) {
        super();
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
