
package com.tema5.pokemonsearchsample.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Prices {

    @SerializedName("holofoil")
    @Expose
    private Holofoil holofoil;
    @SerializedName("reverseHolofoil")
    @Expose
    private ReverseHolofoil reverseHolofoil;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Prices() {
    }

    /**
     * 
     * @param holofoil
     * @param reverseHolofoil
     */
    public Prices(Holofoil holofoil, ReverseHolofoil reverseHolofoil) {
        super();
        this.holofoil = holofoil;
        this.reverseHolofoil = reverseHolofoil;
    }

    public Holofoil getHolofoil() {
        return holofoil;
    }

    public void setHolofoil(Holofoil holofoil) {
        this.holofoil = holofoil;
    }

    public ReverseHolofoil getReverseHolofoil() {
        return reverseHolofoil;
    }

    public void setReverseHolofoil(ReverseHolofoil reverseHolofoil) {
        this.reverseHolofoil = reverseHolofoil;
    }

}
