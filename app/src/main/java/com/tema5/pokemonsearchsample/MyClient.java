package com.tema5.pokemonsearchsample;

import com.tema5.pokemonsearchsample.models.Cards;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Sergio on 16/05/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public interface MyClient {

    String BASE_URL = "https://api.pokemontcg.io/v2/";

    @GET("cards")
    Call<Cards> getAllDatta();
}
