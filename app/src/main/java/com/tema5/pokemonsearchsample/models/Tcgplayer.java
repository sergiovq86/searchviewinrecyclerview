
package com.tema5.pokemonsearchsample.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tcgplayer {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("prices")
    @Expose
    private Prices prices;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Tcgplayer() {
    }

    /**
     * 
     * @param prices
     * @param url
     * @param updatedAt
     */
    public Tcgplayer(String url, String updatedAt, Prices prices) {
        super();
        this.url = url;
        this.updatedAt = updatedAt;
        this.prices = prices;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Prices getPrices() {
        return prices;
    }

    public void setPrices(Prices prices) {
        this.prices = prices;
    }

}
