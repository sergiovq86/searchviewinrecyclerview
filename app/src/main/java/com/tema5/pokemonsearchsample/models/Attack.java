
package com.tema5.pokemonsearchsample.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attack {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("cost")
    @Expose
    private List<String> cost = null;
    @SerializedName("convertedEnergyCost")
    @Expose
    private int convertedEnergyCost;
    @SerializedName("damage")
    @Expose
    private String damage;
    @SerializedName("text")
    @Expose
    private String text;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Attack() {
    }

    /**
     * 
     * @param damage
     * @param cost
     * @param name
     * @param text
     * @param convertedEnergyCost
     */
    public Attack(String name, List<String> cost, int convertedEnergyCost, String damage, String text) {
        super();
        this.name = name;
        this.cost = cost;
        this.convertedEnergyCost = convertedEnergyCost;
        this.damage = damage;
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCost() {
        return cost;
    }

    public void setCost(List<String> cost) {
        this.cost = cost;
    }

    public int getConvertedEnergyCost() {
        return convertedEnergyCost;
    }

    public void setConvertedEnergyCost(int convertedEnergyCost) {
        this.convertedEnergyCost = convertedEnergyCost;
    }

    public String getDamage() {
        return damage;
    }

    public void setDamage(String damage) {
        this.damage = damage;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
